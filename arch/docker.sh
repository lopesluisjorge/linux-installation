#! /bin/bash

sudo pacman -Syu --noconfirm

if ! [[ -x "$(command -v docker)" ]]; then
	sudo pacman -S --noconfirm docker
	sudo usermod -aG docker $USER
	sudo systemctl start docker
	sudo systemctl enable docker
fi
echo "docker installed"

if ! [[ -x "$(command -v docker-compose)" ]]; then
	sudo pacman -S --noconfirm docker-compose
fi
echo "docker-compose installed"
