#! /bin/bash

sudo pacman -Syu --noconfirm

if ! [[ -x "$(command -v "opencv_version")" ]]; then
	sudo pacman -S --noconfirm python-numpy hdf5 gtkglext opencv
fi
echo "opencv installed"
