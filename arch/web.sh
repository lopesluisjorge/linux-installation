#! /bin/bash

sudo pacman -Syu --noconfirm

if ! [[ -x "$(command -v php)" ]]; then
	sudo pacman -S php php-sqlite php-pgsql
fi

if ! [[ -x "$(command -v composer)" ]]; then
	sudo pacman -S composer
fi

if ! [[ -x "$(command -v node)" ]]; then
	sudo pacman -S nodejs
fi

if ! [[ -x "$(command -v npm)" ]]; then
	sudo pacman -S npm
fi
