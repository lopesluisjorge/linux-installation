#! /bin/bash

sudo pacman -Syu --noconfirm

if ! [[ -x "$(command -v pacaur)"  ]]; then
	sudo pacman -S --noconfirm pacaur
fi
echo "pacaur installed"

if ! [[ -x "$(command -v subl)" ]]; then
	curl -O https://download.sublimetext.com/sublimehq-pub.gpg && \
		sudo pacman-key --add sublimehq-pub.gpg && \
		sudo pacman-key --lsign-key 8A8F901A && \
		rm sublimehq-pub.gpg

	echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf

	sudo pacman -Sy --noconfirm sublime-text
fi
echo "sublime-text installed"

if ! [[ -x "$(command -v atom)" ]]; then
	sudo pacman -S --noconfirm atom
fi
echo "atom installed"
