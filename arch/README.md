# Arch Installation

## default applications
```bash
sh init.sh
```

## php and nodejs
```bash
sh web.sh
```

## sublime-text and atom
```bash
sh editors.sh
```

## docker
```bash
sh docker.sh
```

## opencv
```bash
sh opencv.sh
```

## oracle-xe database
```bash
sh oracle-xe.sh
```

## oh-my-fish

### fishshell
```bash
sudo pacman -Syu --noconfirm

sudo pacman -S fish
```

### fonts
```bash
wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf

mkdir -p ~/.fonts/
mkdir -p ~/.config/fontconfig/conf.d
mv PowerlineSymbols.otf ~/.fonts/

fc-cache -vf ~/.fonts/

mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/
```

### oh-my-fish
```bash
curl -L https://get.oh-my.fish | fish

chsh -s /usr/bin/fish

omf install bobthefish shellder budspencer

omf theme bobthefish
```