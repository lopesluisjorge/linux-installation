#! /bin/bash

set -eo pipefail

has() {
	[[ -x "$(command -v "$1")" ]];
}

has_not() {
	! has "$1";
}

sudo pacman -Syu --noconfirm

#
# DEVELOPER TOOLS
#

if has_not vim; then
	sudo pacman -S --noconfirm vim
fi
echo "vim installed"

if has_not git; then
	sudo pacman -S --noconfirm git
fi
echo "git installed"

#
# TERMINALS / TERMINAL TOOLS
#

if has_not terminator; then
	sudo pacman -S --noconfirm terminator
fi
echo "terminator installed"

if has_not htop; then
	sudo pacman -S --noconfirm htop
fi
echo "htop installed"

if has_not iotop; then
	sudo pacman -S --noconfirm iotop
fi
echo "iotop installed"

#
# BROWSERS
#

if has_not firefox; then
	sudo pacman -S --noconfirm firefox
fi
echo "firefox installed"

if has_not chromium; then
	sudo pacman -S --noconfirm chromium
fi
echo "chromium installed"

if has_not postman; then
	pacaur -Syu postman
fi
echo "postman installed"

#
# MULTIMEDIA
#

if has_not audacious; then
	sudo pacman -S --noconfirm audacious
fi
echo "audacious installed"

if has_not vlc; then
	sudo pacman -S --noconfirm vlc
fi
echo "vlc installed"

if has_not viewnior; then
	sudo pacman -S --noconfirm viewnior
fi
echo "viewnior installed"

if has_not gimp; then
	sudo pacman -S --noconfirm gimp
fi
echo "gimp installed"
