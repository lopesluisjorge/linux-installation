#! /bin/bash

sudo pacman -Syu --noconfirm

if ! [[ -x "$(command -v pacaur)"  ]]; then
	sudo pacman -S --noconfirm pacaur
fi
echo "pacaur installed"

if ! [[ -x "$(command -v oracle-xe)"  ]]; then
	pacaur -S --noconfirm oracle-xe
fi
echo "oracle-xe installed"

if ! [[ -x "$(command -v oracle-sqldeveloper)"  ]]; then
	pacaur -S --noconfirm oracle-sqldeveloper
fi
echo "oracle-sqldeveloper installed"
